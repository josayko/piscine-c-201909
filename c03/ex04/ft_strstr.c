/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 12:03:23 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/19 10:38:11 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	char *ptr1;
	char *ptr2;

	if (*to_find == 0)
		return (str);
	while (*str != '\0')
	{
		ptr1 = str;
		ptr2 = to_find;
		while (*ptr1 == *ptr2 && *ptr1 != 0 && *ptr2 != 0)
		{
			ptr1++;
			ptr2++;
		}
		if (*ptr2 == 0)
			return (str);
		str++;
	}
	return (0);
}
