/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: exam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/16 20:17:52 by exam              #+#    #+#             */
/*   Updated: 2019/09/19 16:00:51 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_nbrlen(int nbr)
{
	int count;

	count = 1;
	if (nbr < 0)
		count++;
	while (nbr > 9 || nbr < -9)
	{
		nbr = nbr / 10;
		count++;
	}
	return (count);
}

char	*ft_itoa(int nbr)
{
	char *str;
	int len = ft_nbrlen(nbr);

	str = NULL;
	if(!(str = (char*)malloc(sizeof(char) * len + 1)))
		return (0);
	str[len] = '\0';
	if (nbr > 0)
	{
		while (len >= 0)
		{
			str[len - 1] = nbr % 10 + 48;
			nbr = nbr / 10;
			len--;
		}
	}

	if (nbr < 0)
	{
		if (nbr == -2147483648)
			return(0);
		str[0] = '-';
		nbr = -nbr;
		while (len >= 2)
		{
			str[len - 1] = nbr % 10 + 48;
			nbr = nbr / 10;
			len--;
		}

	}
	return (str);
}

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		int nb = atoi(argv[1]);
		char *str = ft_itoa(nb);
		printf("result: %s\n", str);
	}
}
