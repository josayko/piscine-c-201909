/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 11:47:08 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/19 15:05:16 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int *tab, int n)
{
	int b;
	int aff;

	b = 1;
	aff = 1;
	while (b < n)
	{
		if (tab[b - 1] >= tab[b])
			aff = 0;
		b++;
	}
	if (aff == 0)
		return ;
	b = 0;
	while (b < n)
	{
		ft_putchar(tab[b] + '0');
		b++;
	}
	if (tab[0] < (10 - n))
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_combn(int n)
{
	int a;
	int tab[10];

	a = -1;
	if (n >= 10 || n <= 0)
		return ;
	while (a++ < n)
		tab[a] = a;
	while (tab[0] <= (10 - n))
	{
		ft_putnbr(tab, n);
		tab[n - 1]++;
		a = n;
		while (a != 0 && n > 1)
		{
			a--;
			if (tab[a] > 9)
			{
				tab[a - 1]++;
				tab[a] = 0;
			}
		}
	}
}
