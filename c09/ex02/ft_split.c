/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 18:38:39 by josaykos          #+#    #+#             */
/*   Updated: 2019/10/13 00:23:38 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int		i;
	char	*dest;

	i = 0;
	dest = NULL;
	while (src[i])
		i++;
	if (!(dest = (char*)malloc(sizeof(char) * i + 1)))
		return (0);
	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_replace_sep(char *str, char *sep)
{
	int i;
	int j;

	i = 0;
	while (str[i])
	{
		j = 0;
		while (sep[j])
		{
			if (str[i] == sep[j])
				str[i] = sep[0];
			j++;
		}
		i++;
	}
	return (str);
}

int		ft_count_words(char *str, char c)
{
	int count;
	int is_word;

	count = 0;
	is_word = 0;
	while (*str != '\0')
	{
		if (*str == c)
			is_word = 0;
		else if (is_word == 0)
		{
			is_word = 1;
			count++;
		}
		str++;
	}
	return (count);
}

int		ft_word_len(char *str, int i, char c)
{
	int len;

	len = 0;
	while (str[i] != c && str[i] != '\0')
	{
		len++;
		i++;
	}
	return (len);
}

char	**ft_split(char *str, char *charset)
{
	char	**tab;
	char	*copy;
	int		i;
	int		j;
	int		k;

	tab = NULL;
	i = 0;
	j = 0;
	copy = ft_strdup(str);
	ft_replace_sep(copy, charset);
	tab = malloc(sizeof(char*) * ft_count_words(copy, *charset) + 1);
	while (copy[i] != '\0' && j < ft_count_words(copy, *charset))
	{
		k = 0;
		while (copy[i] == *charset)
			i++;
		tab[j] = malloc(sizeof(char) * ft_word_len(copy, i, *charset) + 1);
		while (copy[i] != *charset && copy[i] != '\0')
			tab[j][k++] = copy[i++];
		tab[j][k] = '\0';
		j++;
	}
	tab[j] = 0;
	return (tab);
}
