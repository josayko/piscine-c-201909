/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 19:22:21 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/11 10:48:19 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_check_base(char *str)
{
	int i;
	int j;
	int len;

	i = 0;
	len = ft_strlen(str);
	if (*str == 0 || len == 1)
		return (0);
	while (i < len - 1)
	{
		j = i;
		if (str[i] == '-' || str[i] == '+' ||
				str[i + 1] == '-' || str[i + 1] == '+')
			return (0);
		while (j <= len - 1)
		{
			if (str[i] != str[j + 1])
				j++;
			else
				return (0);
		}
		i++;
	}
	return (1);
}

void	ft_convert_min(unsigned int nbr, char *base)
{
	unsigned int base_len;

	base_len = ft_strlen(base);
	if (nbr <= base_len - 1)
		ft_putchar(base[nbr]);
	else
	{
		ft_convert_min(nbr / base_len, base);
		ft_convert_min(nbr % base_len, base);
	}
}

void	ft_putnbr_base(int nbr, char *base)
{
	int				base_len;
	unsigned int	min;

	base_len = ft_strlen(base);
	if (ft_check_base(base) == 1)
	{
		if (nbr >= 0 && nbr <= base_len - 1)
			ft_putchar(base[nbr]);
		else if (nbr < 0)
		{
			min = nbr * -1;
			ft_putchar('-');
			ft_convert_min(min, base);
		}
		else
		{
			ft_putnbr_base(nbr / base_len, base);
			ft_putnbr_base(nbr % base_len, base);
		}
	}
}
