/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 15:17:54 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/11 14:48:16 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_index(char c, char *base)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int		ft_check_base(char *str)
{
	int i;
	int j;
	int len;

	i = 0;
	len = ft_strlen(str);
	if (*str == 0 || len == 1)
		return (0);
	while (i < len - 1)
	{
		j = i;
		if (str[i] == '-' || str[i] == '+' || str[i] == ' ' ||
				str[i + 1] == '-' || str[i + 1] == '+' || str[i] == ' ')
			return (0);
		while (j <= len - 1)
		{
			if (str[i] != str[j + 1])
				j++;
			else
				return (0);
		}
		i++;
	}
	return (1);
}

int		ft_atoi_base(char *str, char *base)
{
	int nb;
	int i;
	int	minus_count;

	nb = 0;
	i = 0;
	minus_count = 0;
	if (ft_check_base(base) == 1)
	{
		while ((str[i] >= 9 && str[i] <= 13) || str[i] == ' ')
			i++;
		while (str[i] == '+' || str[i] == '-')
		{
			if (str[i] == '-')
				minus_count++;
			i++;
		}
		while (ft_index(str[i], base) != -1)
			nb = nb * ft_strlen(base) + ft_index(str[i++], base);
		if (minus_count % 2 == 0)
			return (nb);
		else
			return (-nb);
	}
	return (0);
}
