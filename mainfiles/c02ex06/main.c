/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 19:49:21 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/01 13:46:26 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_printable(char *str);

int		main(void)
{
	printf("%d\n", ft_str_is_printable(" 42"));
	printf("%d\n", ft_str_is_printable("Hello	"));
	printf("%d\n", ft_str_is_printable("!"));
	printf("%d\n", ft_str_is_printable("OK"));
	printf("%d\n", ft_str_is_printable("~"));
}
