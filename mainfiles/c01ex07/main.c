/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 21:44:25 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/30 12:23:33 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_rev_int_tab(int *tab, int size);

int        main(void)
{
	int tab[6] = {0,1,2,3,4,5};
	int size = 6;

	ft_rev_int_tab(tab, size);
	int i;

	i = 0;
	while (i <= size-1)
	{
		printf("%d", tab[i]);
		i++;
	}
}
