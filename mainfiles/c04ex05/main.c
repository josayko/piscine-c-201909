/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 15:15:53 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/11 14:34:26 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_atoi_base(char *str, char *base);

int		main(int argc, char **argv)
{
	int ret;

	if (argc == 3)
	{
		ret = ft_atoi_base(argv[1], argv[2]);
		printf("Valeur en Int: %d\n", ret);
	}
	return (0);
}
