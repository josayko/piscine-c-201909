/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 13:50:05 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/01 14:10:34 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strupcase(char *str);

int		main(int argc, char *argv[])
{
	if (argc == 2)
		printf("%s\n", ft_strupcase(argv[1]));
	else
		return (0);
}
