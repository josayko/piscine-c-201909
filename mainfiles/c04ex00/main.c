/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 20:20:45 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/03 20:26:39 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

int		ft_strlen(char *str);

int main () {
	char str[50];
	int len;

	strcpy(str, "This is tutorialspoint.com");

	len = ft_strlen(str);
	printf("Length of |%s| is |%d|\n", str, len);

	return(0);
}
