/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 19:26:32 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 19:37:48 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_lowercase(char *str);

int		main(void)
{
	printf("%d\n", ft_str_is_lowercase("salut"));
	printf("%d\n", ft_str_is_lowercase("42"));
	printf("%d\n", ft_str_is_lowercase("Hello !"));
	printf("%d\n", ft_str_is_lowercase("	OK"));
	printf("%d\n", ft_str_is_lowercase("one4all"));
}
