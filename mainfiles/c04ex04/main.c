/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 19:21:54 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/05 20:08:39 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	ft_putnbr_base(int nbr, char *base);

int		main(int argc, char **argv)
{
	if (argc == 3)
	{
		ft_putnbr_base(atoi(argv[1]), argv[2]);
	}
	else
		return (0);
}
