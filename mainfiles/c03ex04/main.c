/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 11:55:33 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/19 10:39:43 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strstr(char *str, char *to_find);

int main (int argc, char **argv) {

	if(argc == 3)
	{
		char *ret;
		ret = ft_strstr(argv[1], argv[2]);
		printf("The substring is: %s\n", ret);
	}
	return(0);
}
