/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 15:07:09 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 15:36:30 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strcpy(char *dest, char *src);

int		main(void)
{
	char str1[] = "Bonjour";
	char str2[] = "Salut";

	printf("%s\n", str1);
	printf("%s\n", str2);
	printf("%s\n", strcpy(str1, str2));
	printf("%s\n", ft_strcpy(str1, str2));
}
