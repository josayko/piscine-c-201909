/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/02 20:12:15 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/11 11:53:46 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

int		ft_strcmp(char *s1, char *s2);
char	*ft_strcpy(char *dest, char *src);

int main (int argc, char **argv) {
	(void)argc;	
	char str1[25];
	char str2[25];
	int ret;

	strcpy(str1, argv[1]);
	strcpy(str2, argv[2]);

	ret = ft_strcmp(str1, str2);

	if(ret < 0) {
		printf("[%s] is less than [%s]", argv[1], argv[2]);
	} else if(ret > 0) {
		printf("[%s] is less than [%s]", argv[2], argv[1]);
	} else {
		printf("[%s] is equal to [%s]", argv[1], argv[2]);
	}

	return(0);
}
