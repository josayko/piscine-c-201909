/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 17:12:46 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 18:06:39 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_alpha(char *str);

int		main(void)
{
	printf("%d\n", ft_str_is_alpha("Salut"));
	printf("%d\n", ft_str_is_alpha("42"));
	printf("%d\n", ft_str_is_alpha("Hello !"));
	printf("%d\n", ft_str_is_alpha("	OK"));
	printf("%d\n", ft_str_is_alpha("One4All"));
}
