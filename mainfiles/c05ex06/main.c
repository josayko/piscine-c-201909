/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 16:55:25 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/09 18:25:11 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_is_prime(int nb);

int		main(int argc, char **argv)
{
	int nb;

	if (argc == 2)
	{
		nb = ft_is_prime(atoi(argv[1]));
		printf("result: %d\n", nb);
	}
	return (0);
}
