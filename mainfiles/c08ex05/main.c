/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 19:50:13 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/15 10:47:38 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_str.h"

void	ft_show_tab(struct s_stock_str *par);
struct s_stock_str		*ft_strs_to_tab(int ac, char **av);

int		main(void)
{
	char *str1[] = {"Salut", "Bonjour", "Hello"};

	ft_show_tab(ft_strs_to_tab(3, str1));
	return (0);
}
