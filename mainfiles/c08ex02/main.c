/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 17:33:53 by josaykos          #+#    #+#             */
/*   Updated: 2019/10/31 19:56:47 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int		main(int argc, char **argv)
{
	int **tab;
	int nb1;
	int nb2;
	int i;

	i = 0;
	tab = NULL;
	if (argc > 2)
	{
		nb1 = atoi(argv[1]);
		nb2 = atoi(argv[2]);
		if (!(tab = (int **)malloc(sizeof(int*))))
			return (0);
		if (!(*tab = (int *)malloc(sizeof(int) * (nb2 - nb1))))
			return (0);
		printf("%d\n", ft_ultimate_range(tab, nb1, nb2));
		while (i < nb2 - nb1)
		{
			printf("[%d] ", *tab[i]);
			i++;
		}
	}
	return (0);
}
