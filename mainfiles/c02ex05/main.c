/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 19:39:28 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 19:46:46 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_uppercase(char *str);

int		main(void)
{
	printf("%d\n", ft_str_is_uppercase("SALUT"));
	printf("%d\n", ft_str_is_uppercase("42"));
	printf("%d\n", ft_str_is_uppercase("hello"));
	printf("%d\n", ft_str_is_uppercase("	OK"));
	printf("%d\n", ft_str_is_uppercase("one4all"));
}
