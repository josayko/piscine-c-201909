/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 19:49:14 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/10 14:32:10 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include<string.h>

char	*ft_strdup(char *src);

int main(int argc, char **argv)
{
	char *result;
	if (argc == 2)
	{
		result = ft_strdup(argv[1]);
		printf("The string : %s", result);
	}
	return (0);
}
