/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 17:25:45 by josaykos          #+#    #+#             */
/*   Updated: 2019/10/31 19:53:31 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_swap(int *a, int *b);
int		ft_strlen(char *str);
int		ft_strcmp(char *s1, char *s2);

int		main(void)
{
	// TEST
	int a = 18;
	int b = 81;
	printf("a: %d, b: %d\n", a, b);
	ft_swap(&a, &b);
	printf("a: %d, b: %d\n", a, b);
	ft_putchar('J');
	ft_putchar('\n');
	ft_putstr("42");
	ft_putchar('\n');
	ft_putstr("43\n");
	printf("%d\n", ft_strcmp("Bon", "Bonjour"));
	printf("%d\n",ft_strlen("salut"));

	return (0);
}
