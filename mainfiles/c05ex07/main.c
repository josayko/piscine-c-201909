/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 18:26:48 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/09 19:34:00 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_find_next_prime(int nb);

int		main(int argc, char **argv)
{
	int nb;

	nb = ft_find_next_prime((atoi(argv[1])));
	if (argc == 2)
		printf("result: %d\n", nb);
	return (0);
}
