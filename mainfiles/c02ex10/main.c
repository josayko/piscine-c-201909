/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 15:14:15 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/13 19:45:53 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int		main(int argc, char *argv[])
{
	if (argc == 4)
	{
		printf("%lu\n", strlcpy(argv[1], argv[2], atoi(argv[3])));
		printf("%d\n", ft_strlcpy(argv[1], argv[2], atoi(argv[3])));
	}
	else
		return (0);
}
