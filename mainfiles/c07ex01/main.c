/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 13:57:54 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/10 15:20:11 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		*ft_range(int min, int max);

int		main (int argc, char **argv)
{
	if (argc == 3)
	{
		int *tab;
		int nb1;
		int nb2;
		int i;

		i = 0;
		nb1 = atoi(argv[1]);
		nb2 = atoi(argv[2]);
		tab = ft_range(nb1, nb2);
		while (i < nb2 - nb1)
		{
			printf("[%d] ", tab[i]);
			i++;
		}
	}

}
