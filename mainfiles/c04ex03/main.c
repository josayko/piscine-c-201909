/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/04 14:52:04 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/04 18:45:20 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_atoi(char *str);

int main (int argc, char **argv) {
	int val1;
	int val2;

	if (argc == 2)
	{
		val1 = ft_atoi(argv[1]);
		val2 = atoi(argv[1]);
		printf("ft_atoi: String value = %s, Int value = %d\n", argv[1], val1);
		printf("atoi: String value = %s, Int value = %d\n", argv[1], val2);
	}

	else
		return(0);
}
