/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 16:46:55 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/28 17:09:05 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_is_negative(int n);
void	ft_putchar(char c);

int		main(void)
{
	ft_is_negative(5);
	ft_putchar('\n');
	ft_is_negative(-2147483648);
	ft_putchar('\n');
	ft_is_negative(2147483647);
	ft_putchar('\n');
	ft_is_negative(0);
}
