/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 15:22:32 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/09 16:15:40 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_fibonacci(int index);

int		main (int argc, char **argv)
{
	if (argc == 2)
	{
		int nb;

		nb = ft_fibonacci(atoi(argv[1]));
		printf("Fibonacci index %s is %d !\n", argv[1], nb);
	}
	else
		return (0);
}
