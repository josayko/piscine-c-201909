/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/03 18:07:06 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/19 11:08:23 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);

int main (int argc, char **argv)
{
	char *dest;
	char *src;
	int nb;

	dest = argv[1];
	src = argv[2];
	nb = atoi(argv[3]);
	if (argc == 4)
	{
		printf("%d\n", ft_strlcat(dest, src, nb));
		ft_strlcat(dest, src, nb);
		printf("Final destination string : |%s|", dest);
	}
	return(0);
}
