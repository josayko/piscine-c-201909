/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 12:09:06 by josaykos          #+#    #+#             */
/*   Updated: 2019/10/31 19:36:09 by jonny            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	**ft_split(char *str, char *charset);
// TEST
int		main(int argc, char **argv)
{
	int i;

	if (argc == 3)
	{
		char *str1 = argv[1];
		char *str2 = argv[2];
		char **tab;

		tab = ft_split(str1, str2);
		i = 0;
		while(tab[i])
		{
			printf("%s\n", tab[i]);
			i++;
		}
	}
	return (0);
}
