/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 16:18:56 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/10 11:16:49 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int		ft_sqrt(int nb);

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		clock_t t;

		t = clock();
		printf("Result: %d\n", ft_sqrt(atoi(argv[1])));
		t = clock() - t; 
		double time_taken = ((double)t)/CLOCKS_PER_SEC;
		printf("ft_sqrt() took %f seconds to execute \n", time_taken);
	}
	else
		return (0);
}
