/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/17 18:06:47 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/17 18:19:01 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_ultimate_range(int **range, int min, int max);

int		main(int argc, char **argv)
{
	int i;
	int **tab;
	int range;

	if (argc > 2)
	{
		tab = malloc(sizeof(int*));

		i = 0;
		range = ft_ultimate_range(tab, atoi(argv[1]), atoi(argv[2]));
		printf("valeur de return: %d\n", range);
		while (i < range)
		{
			printf("[%d] ", tab[0][i]);
			i++;
		}
	}
	return (0);
}
