/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 15:38:44 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 17:11:45 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strncpy(char *dest, char *src, unsigned int n);

int		main(void)
{
	char str1[] = "Bonjour";
	char str2[] = "Salutations";
	int len = 8;

	printf("%s\n", str1);
	printf("%s\n", str2); 
	printf("%s\n", strncpy(str2, str1, len));
	printf("%s\n", ft_strncpy(str2, str1, len)); 
}
