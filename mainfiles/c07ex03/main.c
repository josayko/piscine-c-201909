/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/12 11:51:41 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/13 18:47:46 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strjoin(int size, char **strs, char *sep);

int		main(void)
{
	char *args[] = {"Un", "Deux", "Trois", "Quatre", "Cinq"};
	char str[] = "()";
	char *join_params;
	join_params = ft_strjoin(5, args, str);
	printf("%s", join_params);
}
