/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 17:17:54 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 09:51:11 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_div_mod(int a, int b, int *div, int *mod);

int		main(void)
{
	int x = 9;
	int y = 2;
	int resultat = 0;
	int reste = 0;

	ft_div_mod(x, y, &resultat, &reste);
	printf("%d\n", resultat);
	printf("%d\n", reste);
}
