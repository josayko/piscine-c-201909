/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 18:09:07 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/31 19:25:56 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_str_is_numeric(char *str);

int		main(void)
{
	printf("%d\n", ft_str_is_numeric("Salut"));
	printf("%d\n", ft_str_is_numeric("42"));
	printf("%d\n", ft_str_is_numeric("Hello !"));
	printf("%d\n", ft_str_is_numeric("	OK"));
	printf("%d\n", ft_str_is_numeric("One4All"));
}
