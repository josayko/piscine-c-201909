/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 14:11:43 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/01 14:27:28 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char	*ft_strlowcase(char *str);

int		main(int argc, char *argv[])
{
	if (argc == 2)
		printf("%s\n", ft_strlowcase(argv[1]));
	else
		return (0);
}
