/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 20:48:09 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/05 21:05:54 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_recursive_factorial(int nb);

int		main(int argc, char **argv)
{
	if (argc == 2)
	{
		int res;

		res = ft_recursive_factorial(atoi(argv[1]));
		printf("%d\n", res);
	}
	else
		return (0);

}
