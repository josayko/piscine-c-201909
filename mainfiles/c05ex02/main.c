/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/09 12:26:12 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/10 11:24:53 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_iterative_power(int nb, int power);

int		main(int argc, char **argv)
{
	if(argc == 3)
	{
		int nb;

		nb = ft_iterative_power(atoi(argv[1]), atoi(argv[2]));
		printf("result: %d\n", nb);
	}
	return (0);
}
