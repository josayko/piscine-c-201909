/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/29 21:21:55 by josaykos          #+#    #+#             */
/*   Updated: 2019/08/29 21:36:58 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_strlen(char *str);

int		main(void)
{
	printf("%d", ft_strlen("TEST"));
	printf("%d\n", ft_strlen("OK"));
	// test whitespace and tab (2)
	printf("%d", ft_strlen(" 	"));
}
