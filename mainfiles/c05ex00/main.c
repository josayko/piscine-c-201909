/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/05 18:16:04 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/05 19:41:49 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_iterative_factorial(int nb);

int		main(int argc, char **argv)
{
	int nb;
	if (argc == 2)
	{
		nb = ft_iterative_factorial(atoi(argv[1]));
		printf("%d\n", nb);
	}
	else
		return (0);
}
