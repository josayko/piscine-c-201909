/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 16:07:27 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/17 18:19:05 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	int i;
	int size;

	i = 0;
	size = max - min;
	if (min >= max)
	{
		range = NULL;
		return (0);
	}
	if (!(*range = (int *)malloc(sizeof(int) * (max - min))))
		return (-1);
	while (i < size)
	{
		range[0][i] = min;
		i++;
		min++;
	}
	return (i);
}
