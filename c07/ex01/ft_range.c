/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/10 13:57:09 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/11 16:28:23 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int *tab;
	int range;
	int i;

	tab = NULL;
	i = 0;
	if (min >= max)
		return (tab);
	range = max - min;
	if (!(tab = (int *)malloc(sizeof(int) * range)))
		return (0);
	while (i <= range - 1)
	{
		tab[i] = min;
		i++;
		min++;
	}
	return (tab);
}
