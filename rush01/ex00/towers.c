/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   towers.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/07 15:25:01 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/08 22:44:35 by wpigny           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "towers.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_display_grid(char *str)
{
	char	**grid;
	char	nb;
	int		i;
	int		j;

	i = 0;
	nb = '0';
	grid = (char **)malloc(sizeof(char*) * 6);
	while (i < 6)
	{
		grid[i] = (char *)malloc(sizeof(char) * 6);
		i++;
	}
	ft_param_in_grid(grid, str);
	j = 1;
	while (j >= 1 && j <= 4)
	{
		i = 1;
		nb = '0';
		while (i >= 1 && i <= 4)
		{
			grid[i][j] = nb;
			i++;
		}
		j++;
	}
	j = 1;
	nb = '1';
	while (j <= 4)
	{
		i = 1;
		while (i <= 4)
		{
			if (grid[i][j] == '0')
			{
				grid[i][j] = ft_put_nbr_in_case(grid, nb, i, j);
				nb = ft_put_nbr_in_case(grid, nb, i, j);
			}
			i++;
		}
		j++;
	}
	j = 1;
	while (j < 5)
	{
		i = 1;
		while (i < 5)
		{
			ft_putchar(grid[i][j]);
			ft_putchar(' ');
			i++;
		}
		ft_putchar('\n');
		j++;
	}
}
