/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   towers.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 12:11:54 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/08 18:42:53 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOWERS_H
# define TOWERS_H

char	ft_put_nbr_in_case(char **tab, char nb, char k, char l);
void	ft_display_grid(char *str);
void	ft_param_in_grid(char **tab, char *str);

#endif
