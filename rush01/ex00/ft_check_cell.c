/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_KetL.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 18:41:30 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/08 19:01:15 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_check_ligne(char **tab, char nb, int k, int l)
{
	int temp;

	temp = k + 1;
	tab[k][l] = nb;
	while (tab[k][l] != tab[temp][l] && temp <= 4)
	{
		temp++;
	}
	if (tab[k][l] == tab[temp][l])
		return (0);
	return (1);
}

char	ft_put_nbr_in_case(char **tab, char nb, int k, int l)
{
	if (ft_check_ligne(tab, nb, k, l) == 1)
		return (nb);
	if (ft_check_ligne(tab, nb, k, l) == 0)
		nb++;
	return (nb);
}
