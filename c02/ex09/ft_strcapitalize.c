/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 14:30:42 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/03 17:58:19 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_alphanum(char c)
{
	if (c < 48 || (c > 57 && c < 65) || (c > 90 && c < 97) || c > 122)
		return (0);
	return (1);
}

int		ft_str_is_lowercase(char c)
{
	if (c < 97 || c > 122)
		return (0);
	return (1);
}

int		ft_str_is_uppercase(char c)
{
	if (c < 65 || c > 90)
		return (0);
	return (1);
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (i == 0 && ft_str_is_lowercase(str[i]) == 1)
			str[i] = str[i] - 32;
		else if (i > 0 && ft_str_is_alphanum(str[i - 1]) == 0
				&& ft_str_is_lowercase(str[i]) == 1)
			str[i] = str[i] - 32;
		else if (i > 0 && ft_str_is_alphanum(str[i - 1]) == 1
				&& ft_str_is_uppercase(str[i]) == 1)
			str[i] = str[i] + 32;
		i++;
	}
	return (str);
}
