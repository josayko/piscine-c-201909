cat /etc/passwd | grep -v "#" | rev | sed '1d; n; d' | cut -d ":" -f7 | sort -r | sed -n "$FT_LINE1, $FT_LINE2 p" | tr "\n" "," | sed "s/,/, /g" | sed "s/, $/./" | tr -d "\n"
