/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 16:35:06 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/15 21:36:18 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"

char	*ft_index_init(int size, char *tab, char *buffer)
{
	int i;
	int fd;

	i = 0;
	if (!(tab = (char *)malloc(sizeof(char) * (size + 1))))
		return (0);
	fd = open("./dict/numbers.dict", O_RDONLY);
	read(fd, buffer, size);
	while (buffer[i])
	{
		tab[i] = buffer[i];
		i++;
	}
	tab[i] = '\0';
	return (tab);
}

char	*ft_check_dico(char *tab)
{
	int		fd;
	int		i;
	int		dico_len;
	char	buf[1000];

	i = 0;
	dico_len = 0;
	tab = NULL;
	fd = open("./dict/numbers.dict", O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("Error\n");
		return (0);
	}
	while (read(fd, buf, 1) != 0)
		dico_len++;
	buf[dico_len] = '\0';
	tab = ft_index_init(dico_len, tab, buf);
	if (close(fd) == -1)
	{
		ft_putstr("Error");
		return (0);
	}
	return (tab);
}

int		ft_parse(char *nbr)
{
	char *dico;

	dico = NULL;
	ft_check_nbr(nbr, ft_check_dico(dico));
	return (0);
}
