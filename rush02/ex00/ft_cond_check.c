/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cond_check.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mabriand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 14:40:22 by mabriand          #+#    #+#             */
/*   Updated: 2019/09/15 18:44:15 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"

int		cond_check(int argc, char **argv)
{
	int i;

	i = 0;
	if (argc > 3 || argc < 2)
	{
		ft_putstr("Error\n");
		return (0);
	}
	while (argv[argc - 1][i] != '\0')
	{
		if (argv[argc - 1][i] > '9' || argv[argc - 1][i] < '0')
		{
			ft_putstr("Error\n");
			return (0);
		}
		i++;
	}
	return (1);
}
