/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_nbr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/15 16:26:02 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/15 21:36:13 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"

char	*ft_check_nbr(char *nbr, char *dict)
{
	int		i;
	char	*str;
	char	*index[20];

	index[0] = "zero";
	index[1] = "one";
	index[2] = "two";
	index[3] = "three";
	index[4] = "four";
	index[5] = "five";
	index[6] = "six";
	index[7] = "seven";
	index[8] = "eight";
	index[9] = "nine";
	str = NULL;
	i = 0;
	str = index[(nbr[i] - 48)];
	ft_putstr(str);
	return (dict);
}
