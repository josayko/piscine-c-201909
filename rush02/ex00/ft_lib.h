/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lib.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 12:26:48 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/15 21:36:21 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIB_H
# define FT_LIB_H
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <fcntl.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(long int nb);
int		cond_check(int argc, char **argv);
int		ft_parse(char *str);
int		ft_strlen(char *str);
int		ft_cutnbr(int len);
char	*ft_check_nbr(char *nbr, char *dict);
char	*ft_strcat(char *dest, char *src);

#endif
