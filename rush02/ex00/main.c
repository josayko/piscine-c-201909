/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/14 12:37:24 by josaykos          #+#    #+#             */
/*   Updated: 2019/09/15 18:40:57 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lib.h"
#include <stdio.h>

int		main(int ac, char **av)
{
	if (cond_check(ac, av) == 1)
	{
		ft_parse(av[1]);
	}
	else
		return (0);
}
